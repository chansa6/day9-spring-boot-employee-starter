package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        employeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeRepository.findAllByGender(gender);
    }

    public Employee create(Employee employee) {
        return employeeRepository.save(employee);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findAll(PageRequest.of(pageNumber - 1, pageSize)).getContent();
    }

    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }

    public List<Employee> findAllByAgeLessThan(Integer upperAge) {
        return employeeRepository.findAllByAgeLessThan(upperAge);
    }

    public List<Employee> findAllByAgeBetween(Integer minAge, Integer maxAge) {
        return employeeRepository.findAllByAgeBetween(minAge, maxAge);
    }

    public List<Employee> findAllByNameContaining(String nameSubstring) {
        return employeeRepository.findAllByNameContaining(nameSubstring);
    }
}
